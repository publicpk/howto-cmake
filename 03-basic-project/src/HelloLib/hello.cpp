#include <string.h>

#include "hello.h"
#include "aloha.h"

const char* hello(char* strbuf) {
    strcpy(strbuf, "Hello,");
    strcat(strbuf, aloha());
    return strbuf;
}
