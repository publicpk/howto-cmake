
cmake_minimum_required (VERSION 2.8.11)

# the root source directory: ${HelloLib_SOURCE_DIR}
# the root binary directory: ${HelloLib_BINARY_DIR}
project (HelloLib)

set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH};${CMAKE_CURRENT_SOURCE_DIR}/cmake")

include(Configure)

add_subdirectory (AlohaLib)

add_library (Hello STATIC hello.cpp)

target_include_directories (Hello PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

target_link_libraries (Hello LINK_PUBLIC Aloha)
