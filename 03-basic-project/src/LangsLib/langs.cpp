#include <string.h>
#include "langs.h"

const char* greetingLang(const char* str) {
    if ( strcmp(str, "Hello") == 0 )
        return "American";

    if ( strcmp(str, "Aloha") == 0 )
        return "Hawaiian";
}
