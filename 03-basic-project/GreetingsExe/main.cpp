
#include <iostream>
#include "hello.h"
#include "langs.h"

using namespace std;

void printLang(const char* s) {
    cout << s << ":" << greetingLang(s) << endl;
}

int main() {
    char strbuf[100];
    const char * greetings = hello(strbuf);
    cout << greetings << endl;

    char* s, *c;
    c = s = strbuf;
    while ( *c++ ) {
        if ( *c == ',' ) {
            *c = 0;
            printLang(s);
            s = c+1;
        }
    }
    printLang(s);
    
    return 0;
}
