
# HowTo [CMake](https://cmake.org) #
  - Step-by-step instructions for CMake usages from simple to complex
  - So far, there is no complex one yet but there will be soon. :)
  
## Another CMake Reference
  - See [this 'android-libs'](https://bitbucket.org/publicpk/android-libs) repo.
