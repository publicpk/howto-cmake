#!/bin/bash

mkdir -p ./build

pushd ./build

echo ""
echo ">>> GENERATE Build scripts *******************"
cmake -g "Unix Makefiles" ..

echo ""
echo ">>> BUILD Project ****************************"
#VERBOSE=1 make
make

echo ""
echo ">>> Check Output *****************************"
./HelloExe/hello

echo ""
echo ">>> CLEAN Project ****************************"
#make clean

popd
