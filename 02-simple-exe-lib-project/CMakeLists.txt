
cmake_minimum_required (VERSION 2.8.11)
project (MyHello)

# Recurse into the "HelloLib" and "HelloExe" subdirectories.
# This does not actually cause another cmake executable to run.
# The same process will walk through
# the project's entire directory structure.
add_subdirectory (HelloLib)
add_subdirectory (HelloExe)

