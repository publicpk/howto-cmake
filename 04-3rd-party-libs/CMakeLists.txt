
# See https://github.com/pyke369/sffmpeg/blob/master/CMakeLists.txt

cmake_minimum_required (VERSION 3.0)
project (ExternalLibBuild)

# tiff
set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH};${CMAKE_CURRENT_SOURCE_DIR}/3rd-parties/tiff")
include(BuildTIFFLib)

# png16
set(CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH};${CMAKE_CURRENT_SOURCE_DIR}/3rd-parties/png")
include(BuildPng16Lib)

