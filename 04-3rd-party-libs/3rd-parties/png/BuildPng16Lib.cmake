
cmake_minimum_required (VERSION 3.0)

include(ExternalProject)

# For the information about Version and Download
# see http://www.libpng.org/pub/png/libpng.html

set(LIB_VERSION "1.6.29")
set(LIB_NAME "png-${LIB_VERSION}")
set(LIB_URL
  "https://ftp-osl.osuosl.org/pub/libpng/src/libpng16/libpng-${LIB_VERSION}.tar.xz")
set(LIB_MD5 "3245dbd76ea91e1437507357b858ec97")

set(LIB_BASE_DIR "${CMAKE_CURRENT_BINARY_DIR}/3rd-parties")
set(LIB_INST_DIR "${LIB_BASE_DIR}/installed")

message(">>> Building ${LIB_NAME}")
message("   - URL: ${LIB_URL}")

ExternalProject_Add(
    ${LIB_NAME}
    URL ${LIB_URL}
    URL_HASH MD5=${LIB_MD5}
    DOWNLOAD_DIR ${LIB_BASE_DIR}
    PREFIX ${LIB_BASE_DIR}
    SOURCE_DIR ${LIB_BASE_DIR}/src/${LIB_NAME}
    BUILD_IN_SOURCE 1
    CONFIGURE_COMMAND ./configure --prefix=${LIB_INST_DIR}
    BUILD_COMMAND make
    INSTALL_COMMAND make install
)

