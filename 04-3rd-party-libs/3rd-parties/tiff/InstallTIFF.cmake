
cmake_minimum_required (VERSION 3.0)

include(ExternalProject)

set(LIB_VERSION "3.8.2")
set(LIB_NAME "tiff-${LIB_VERSION}")
set(LIB_URL "http://dl.maptools.org/dl/libtiff/tiff-${LIB_VERSION}.tar.gz")

set(LIB_BASE_DIR "${CMAKE_CURRENT_BINARY_DIR}/3rd-parties")
set(LIB_INST_DIR "${LIB_BASE_DIR}/install")

message(">>> Building ${LIB_NAME}")
message("   - URL: ${LIB_URL}")

ExternalProject_Add(
    ${LIB_NAME}
    URL ${LIB_URL}
    DOWNLOAD_DIR ${LIB_BASE_DIR}
    PREFIX ${LIB_BASE_DIR}
    SOURCE_DIR ${LIB_BASE_DIR}/${LIB_NAME}
    BUILD_IN_SOURCE 1
    CONFIGURE_COMMAND ./configure --prefix=${LIB_INST_DIR}
    BUILD_COMMAND make
    INSTALL_COMMAND make install
)

