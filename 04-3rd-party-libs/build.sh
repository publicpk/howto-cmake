#!/bin/bash

mkdir -p ./build

pushd ./build

echo ""
echo ">>> GENERATE Build scripts *******************"
cmake -G "Unix Makefiles" ..

echo ""
echo ">>> BUILD Project ****************************"
cmake --build .

echo ""
echo ">>> Check Output *****************************"
#ls -al output_file

echo ""
echo ">>> CLEAN Project ****************************"
#make clean

popd
