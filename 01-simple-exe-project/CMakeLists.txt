cmake_minimum_required (VERSION 2.8.11)

# the root source directory: ${HelloExe_SOURCE_DIR}
# the root binary directory: ${HelloExe_BINARY_DIR}
project (HelloExe)

# Add executable called "hello" that is built from the source files
# The extensions are automatically found.
add_executable (hello main.cpp)
