#!/bin/bash

mkdir -p ./build

pushd ./build

echo ""
echo ">>> GENERATE Build scripts *******************"
cmake -g "Unix Makefiles" ..

echo ""
echo ">>> BUILD Project ****************************"
make

echo ""
echo ">>> RUN Exe **********************************"
./hello

echo ""
echo ">>> CLEAN Project ****************************"
#make clean

popd
