#!/bin/bash

mkdir -p ./build

pushd ./build

echo ">>> GENERATE Build scripts *******************"
cmake -g "Unix Makefiles" ..

echo ""
echo ">>> BUILD Project ****************************"
make

echo ""
echo ">>> CHECK Output *****************************"
ls -al libHello.a

echo ""
echo ">>> CLEAN Project ****************************"
#make clean

popd
